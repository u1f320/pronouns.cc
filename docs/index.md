# pronouns.cc

pronouns.cc is a service where you can create a list of your preferred names, pronouns, and other terms, and share it with other people.

*Note: this documentation site is a work in progress, and currently only contains (partial) API documentation.*

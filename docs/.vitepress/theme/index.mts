import DefaultTheme from 'vitepress/theme-without-fonts'
import "@fontsource/firago/400.css";
import "@fontsource/firago/400-italic.css";
import "@fontsource/firago/700.css";
import "@fontsource/firago/700-italic.css";
import "@fontsource/fira-mono";
import "./custom.css";

export default DefaultTheme

# Contact

If you have any questions about pronouns.cc, you can email [contact@pronouns.cc](mailto:contact@pronouns.cc).  
You can also contact this address if your account has been suspended and you want your data exported.

# Licenses & attribution

- The pronouns.cc logo uses [Twitter's Twemoji](https://github.com/twitter/twemoji) and the [Fira Sans](https://bboxtype.com/typefaces/FiraGO) font.
- The font used throughout the website is [FiraGO](https://bboxtype.com/typefaces/FiraGO).

pronouns.cc itself is licensed under the GNU Affero General Public License, version 3 or later.  
Here's the blurb that the license recommends me to put somewhere in the program:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

pronouns.cc's source code is available [on Codeberg](https://codeberg.org/pronounscc/pronouns.cc).

# Support pronouns.cc

If you like pronouns.cc and want to support it, I have a Liberapay page here: [liberapay.com/u1f320](https://liberapay.com/u1f320/)!  
It's not required and doesn't give you any perks, but it would be helpful—running a website is expensive.

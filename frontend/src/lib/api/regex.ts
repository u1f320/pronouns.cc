export const memberNameRegex = /^[^@\\?!#/\\\\[\]"\\{\\}'$%&()+<=>^|~`,]{1,100}$/;
export const usernameRegex = /^[\w-.]{2,40}$/;

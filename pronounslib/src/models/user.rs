#[derive(Debug)]
pub struct User {
    pub id: String,
    pub sid: String,
    pub username: String,
}
